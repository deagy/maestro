package model

import "encoding/json"

type Rule struct {
	Id        string
	When      json.RawMessage
	Condition json.RawMessage
	Action    json.RawMessage
}
